skinDb = {};
skinDb.skins = {};
skinDb.page = 1;
skinDb.pages = 0;--this will be overwritten if we know how much pages are there
skinDb.outformat = "hex";
--this function fetches all information from the skin DB
skinDb.fetchAll = function(address)
local page = skinDb.page;
local outformat = "hex"; --can be hex or base64
local pages = 0;
repeat
	minetest.log("action","DOWNLOADING SKIN INFORMATION "..page.." of "..pages.." this may take a while");
	content = http.request(address.."?getlist&outformat="..outformat.."&page="..page);
	--minetest.log("info","DOWNLOAD COMPLETE");
	local json = minetest.parse_json(content);
	skinDb.skins = TableConcat(skinDb.skins,json.skins);
	--minetest.log("info","there are now "..table.getn(all_skins).." skins downloaded");
	pages = json.pages;
	page = page+1;
	
until page > pages;
minetest.log("info","All "..table.getn(skinDb.skins).." skins downloaded from Skin DB :-)");
end


skinDb.saveAll = function(path)
minetest.log("action","generating textures folder "..path .." if not exits");
os.execute("mkdir "..path)
minetest.log("action","clearing textures folder "..path .." if not empty");
os.execute("rm "..path.."/*")
	for skin = 1, table.getn(skinDb.skins),1 do
		local current = skinDb.skins[skin];
		local skin_name = get_skin_name(current)
		minetest.log("action","saving skin " ..skin_name);	 
		local file = io.open(path.."/"..skin_name, "w");
		if skinDb.outformat == "base64" then
		file:write(dec(current.img));
		else
		file:write((current.img):fromhex());
		end
		file:close();
	end
end

skinDb.register_for_trader = function()
	local skin_names = {};
	for skin = 1, table.getn(skinDb.skins),1 do
		local current = skinDb.skins[skin];
		local skin_name = get_skin_name(current)
		skin_names[skin] = skin_name;				
	end
	mob_basics.TEXTURES = TableConcat(mob_basics.TEXTURES,skin_names);
end

function TableConcat(t1,t2)
    for i=1,#t2 do
        t1[#t1+1] = t2[i]
    end
    return t1
end
local escape = function(seq)
seq = string.gsub(seq,"[%c,%/,%),%(]", "");--%c sind alle steuerzeichen
seq = string.gsub(seq,"%s", "_"); --%s sind alle leerzeichen
seq = string.gsub(seq,"%z", "-")
seq = string.gsub(seq,"[%&,%;]", "");--%z characker with presentation 0
--seq = string.gsub(seq,'"', "-");
--seq = string.gsub(seq,';', "-");
--seq = string.gsub(seq,'&', "-");
--seq = minetest.formspec_escape(seq);
return seq;
end

function string.fromhex(str)
    return (str:gsub('..', function (cc)
        return string.char(tonumber(cc, 16))
    end))
end

function string.tohex(str)
    return (str:gsub('.', function (c)
        return string.format('%02X', string.byte(c))
    end))
end

function get_skin_name(skin)
return escape(skin.name.."_by_"..skin.author..".png");
end
